export const MOTW = {};

MOTW.attributes = {
  charm: "MOTW.Charm",
  cool: "MOTW.Cool",
  sharp: "MOTW.Sharp",
  tough: "MOTW.Tough",
  weird: "MOTW.Weird",
};
