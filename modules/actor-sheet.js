export class MotwActorSheet extends ActorSheet {
  constructor(...args) {
    super(...args);
  }
  /**
   * Extend and override the default options used by the 5e Actor Sheet
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["motw", "char-sheet"],
      template: "systems/motw/templates/actor-sheet.html",
      width: 600,
      height: 600,
      tabs: [
        {
          navSelector: ".tabs-left",
          contentSelector: ".tab-left",
          initial: "equipment",
        },
        {
          navSelector: ".tabs-right",
          contentSelector: ".tab-right",
          initial: "moves",
        },
      ],
    });
  }

  _prepareItems(data) {
    // Partition items by category
    let [inventory, moves] = data.items.reduce(
      (arr, item) => {
        // Classify items into types
        if (item.type === "item") arr[0].push(item);
        else if (item.type === "move") arr[1].push(item);
        return arr;
      },
      [[], [], [], []]
    );
    // Assign and return
    data.inventory = inventory;
    data.moves = moves;
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for rendering the Actor sheet
   * The prepared data object contains both the actor data as well as additional sheet options
   */
  getData() {
    const data = super.getData();
    data.config = CONFIG.MOTW;
    // Prepare owned items
    this._prepareItems(data);
    return data;
  }

  _createEditor(target, editorOptions, initialContent) {
    editorOptions.toolbar = "styleselect bullist hr image removeFormat save";
    editorOptions.height = 240;
    super._createEditor(target, editorOptions, initialContent);
  }
  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
  _onItemSummary(event) {
    event.preventDefault();
    let li = $(event.currentTarget).parents(".item-entry"),
      expanded = !li.children(".collapsible").hasClass("collapsed");
    li = $(li);
    let ol = li.children(".collapsible");
    let icon = li.find("i.fas");

    // Collapse the Playlist
    if (expanded) {
      ol.slideUp(200, () => {
        ol.addClass("collapsed");
        icon.removeClass("fa-angle-up").addClass("fa-angle-down");
      });
    }

    // Expand the Playlist
    else {
      ol.removeClass("collapsed");
      ol.slideDown(200, () => {
        icon.removeClass("fa-angle-down").addClass("fa-angle-up");
      });
    }
  }

  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Rolls
    html.find(".attribute label a").click((ev) => {
      let attId =
        event.currentTarget.parentElement.parentElement.dataset.attributeId;
      this.actor.roll(attId, { event: event });
    });

    // Counters
    html.find(".counter-data").click((ev) => {
      let id = ev.currentTarget.parentElement.dataset.fieldId;
      let delta = ev.currentTarget.classList.contains("active") ? -1 : 1;
      this.actor.counterIncrement(id, delta);
    });

    html.find(".checkbox-data").click((ev) => {
      let id = ev.currentTarget.parentElement.dataset.fieldId;
      let newData = {};
      let field = `data.${id}`;
      setProperty(newData, field, !getProperty(this.actor.data, field));
      this.actor.update(newData);
    });

    // Update Inventory Item
    html.find(".item-edit").click((ev) => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find(".item-delete").click((ev) => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    html.find(".item-name").click((event) => {
      this._onItemSummary(event);
    });
  }

  async _onResize(event) {
    super._onResize(event);
    let html = $(event.path[2]);
    let editors = html.find(".motw_panel_content");
    editors.css("height", this.position.height - 340);
  }
}
