/* -------------------------------------------- */

/**
 * Extend the basic ItemSheet with some very simple modifications
 */
export class MotwItemSheet extends ItemSheet {
  constructor(...args) {
    super(...args);
  }

  /**
   * Extend and override the default options used by the Simple Item Sheet
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["motw", "sheet", "item"],
      width: 520,
      height: 480,
    });
  }

  get template() {
      const path = "systems/motw/templates/items/";
      return `${path}/${this.item.data.type}-sheet.html`;
  }
}
